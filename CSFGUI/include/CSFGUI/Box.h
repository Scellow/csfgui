#ifndef CSFGUI_BOX_H
#define CSFGUI_BOX_H

#include <CSFGUI/Export.h>
#include <CSFGUI/Types.h>

typedef enum sfgBoxOrientation : sfUint8
{
    Horizontal = 0,
    Vertical = 1
};

CSFGUI_API sfgBox* sfgBox_Create(sfgBoxOrientation Orientation, float Spacing);
CSFGUI_API void sfgBox_Destroy(sfgBox* Box);
CSFGUI_API float sfgBox_GetSpacing(sfgBox* Box);
CSFGUI_API void sfgBox_Pack(sfgBox* Box, sfgWidget* Widget, sfBool Expand, sfBool Fill);
CSFGUI_API void sfgBox_PackStart(sfgBox* Box, sfgWidget* Widget, sfBool Expand, sfBool Fill);
CSFGUI_API void sfgBox_PackEnd(sfgBox* Box, sfgWidget* Widget, sfBool Expand, sfBool Fill);
CSFGUI_API void sfgBox_SetSpacing(sfgBox* Box, float Spacing);
CSFGUI_API sfBool sfgBox_IsEqual(sfgBox* A, sfgBox* B);
CSFGUI_API sfgContainer* sfgBox_GetBaseContainer(sfgBox* Box);

#endif
