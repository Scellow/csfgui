#ifndef CSFGUI_TYPES_H
#define CSFGUI_TYPES_H

#include <SFML/Config.h>
#include <string>

typedef struct sfgSFGUI sfgSFGUI;
typedef struct sfgObject sfgObject;
typedef struct sfgWindow sfgWindow;
typedef struct sfgWidget sfgWidget;
typedef struct sfgContainer sfgContainer;
typedef struct sfgRenderer sfgRenderer;
typedef struct sfgRendererViewport sfgRendererViewport;
typedef struct sfgDesktop sfgDesktop;
typedef struct sfgEngine sfgEngine;
typedef struct sfgBin sfgBin;
typedef struct sfgLabel sfgLabel;
typedef struct sfgBox sfgBox;
typedef struct sfgSignal sfgSignal;
typedef struct sfgButton sfgButton;
typedef struct sfgImage sfgImage;
typedef struct sfgToggleButton sfgToggleButton;
typedef struct sfgCheckButton sfgCheckButton;
typedef struct sfgRadioButton sfgRadioButton;
typedef struct sfgRadioButtonGroup sfgRadioButtonGroup;

#endif
