#ifndef CSFGUI_RENDERER_H
#define CSFGUI_RENDERER_H

#include <CSFGUI/Export.h>
#include <CSFGUI/Types.h>

CSFGUI_API void sfgRenderer_Destroy(sfgRenderer* Renderer);
CSFGUI_API sfBool sfgRenderer_IsEqual(sfgRenderer* A, sfgRenderer* B);

#endif
