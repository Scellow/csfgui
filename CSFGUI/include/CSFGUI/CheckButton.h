#ifndef CSFGUI_CHECKBUTTON_H
#define CSFGUI_CHECKBUTTON_H

#include <CSFGUI/Export.h>
#include <CSFGUI/Types.h>

CSFGUI_API sfgCheckButton* sfgCheckButton_Create(const char* Label);
CSFGUI_API void sfgCheckButton_Destroy(sfgCheckButton* CheckButton);
CSFGUI_API sfgToggleButton* sfgCheckButton_GetBaseToggleButton(sfgCheckButton* CheckButton);
CSFGUI_API sfBool sfgCheckButton_IsEqual(sfgCheckButton* A, sfgCheckButton* B);

#endif
