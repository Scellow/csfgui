#ifndef CSFGUI_RENDERERVIEWPORT_H
#define CSFGUI_RENDERERVIEWPORT_H

#include <CSFGUI/Export.h>
#include <CSFGUI/Types.h>

CSFGUI_API void sfgRendererViewport_Destroy(sfgRendererViewport* RendererViewport);
CSFGUI_API sfBool sfgRendererViewport_IsEqual(sfgRendererViewport* A, sfgRendererViewport* B);

#endif
