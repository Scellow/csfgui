#ifndef CSFGUI_BUTTON_H
#define CSFGUI_BUTTON_H

#include <CSFGUI/Export.h>
#include <CSFGUI/Types.h>

CSFGUI_API sfgButton* sfgButton_Create(const char* Label);
CSFGUI_API void sfgButton_Destroy(sfgButton* Button);
CSFGUI_API sfgBin* sfgButton_GetBaseBin(sfgButton* Button);
CSFGUI_API void sfgButton_ClearImage(sfgButton* Button);
CSFGUI_API sfgImage* sfgButton_GetImage(sfgButton* Button);
CSFGUI_API void sfgButton_SetImage(sfgButton* Button, sfgImage* Image);
CSFGUI_API const char* sfgButton_GetLabel(sfgButton* Button);
CSFGUI_API void sfgButton_SetLabel(sfgButton* Button, const char* Label);
CSFGUI_API sfBool sfgButton_IsEqual(sfgButton* A, sfgButton* B);

#endif
