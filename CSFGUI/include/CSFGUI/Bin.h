#ifndef CSFGUI_BIN_H
#define CSFGUI_BIN_H

#include <CSFGUI/Export.h>
#include <CSFGUI/Types.h>

CSFGUI_API void sfgBin_Destroy(sfgBin* Bin);
CSFGUI_API sfgWidget* sfgBin_GetChild(sfgBin* Bin);
CSFGUI_API sfgContainer* sfgBin_GetBaseContainer(sfgBin* Bin);
CSFGUI_API sfBool sfgBin_IsEqual(sfgBin* A, sfgBin* B);

#endif
