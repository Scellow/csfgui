#ifndef CSFGUI_SIGNALID_H
#define CSFGUI_SIGNALID_H

#include <CSFGUI/Export.h>
#include <CSFGUI/Types.h>
#include <cstddef>

typedef enum sfgSignalIDType : sfInt32
{
    Widget_OnStateChange = 1,
    Widget_OnGainFocus = 2,
    Widget_OnLostFocus = 3,
    Widget_OnExpose = 4,
    Widget_OnSizeAllocate = 5,
    Widget_OnSizeRequest = 6,
    Widget_OnMouseEnter = 7,
    Widget_OnMouseLeave = 8,
    Widget_OnMouseMove = 9,
    Widget_OnMouseLeftPress = 10,
    Widget_OnMouseRightPress = 11,
    Widget_OnMouseLeftRelease = 12,
    Widget_OnMouseRightRelease = 13,
    Widget_OnLeftClick = 14,
    Widget_OnRightClick = 15,
    Widget_OnKeyPress = 16,
    Widget_OnKeyRelease = 17,
    Widget_OnText = 18,

    ToggleButton_OnToggle = 19
};

CSFGUI_API std::size_t sfgSignalID_GetID(sfgObject* Object, sfgSignalIDType SignalIDType);

#endif