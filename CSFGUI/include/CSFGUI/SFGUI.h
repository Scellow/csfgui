#ifndef CSFGUI_SFGUI_H
#define CSFGUI_SFGUI_H

#include <CSFGUI/Export.h>
#include <CSFGUI/Types.h>
#include <SFML/Window/Window.h>
#include <SFML/Graphics/RenderWindow.h>
#include <SFML/Graphics/RenderTexture.h>

CSFGUI_API sfgSFGUI* sfgSFGUI_Create();
CSFGUI_API void sfgSFGUI_Destroy(sfgSFGUI* SFGUI);
CSFGUI_API void sfgSFGUI_DisplayWindow(sfgSFGUI* SFGUI, sfWindow* Window);
CSFGUI_API void sfgSFGUI_DisplayRenderWindow(sfgSFGUI* SFGUI, sfRenderWindow* RenderWindow);
CSFGUI_API void sfgSFGUI_DisplayRenderTexture(sfgSFGUI* SFGUI, sfRenderTexture* RenderTexture);
CSFGUI_API sfgRenderer* sfgSFGUI_GetRenderer(sfgSFGUI* SFGUI);

#endif
