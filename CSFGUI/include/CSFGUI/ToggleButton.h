#ifndef CSFGUI_TOGGLEBUTTON_H
#define CSFGUI_TOGGLEBUTTON_H

#include <CSFGUI/Export.h>
#include <CSFGUI/Types.h>

CSFGUI_API sfgToggleButton* sfgToggleButton_Create(const char* Label);
CSFGUI_API void sfgToggleButton_Destroy(sfgToggleButton* ToggleButton);
CSFGUI_API sfgButton* sfgToggleButton_GetBaseButton(sfgToggleButton* ToggleButton);
CSFGUI_API sfBool sfgToggleButton_IsEqual(sfgToggleButton* A, sfgToggleButton* B);
CSFGUI_API sfBool sfgToggleButton_IsActive(sfgToggleButton* ToggleButton);
CSFGUI_API void sfgToggleButton_SetActive(sfgToggleButton* ToggleButton, sfBool Active);

#endif