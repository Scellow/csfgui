#ifndef CSFGUI_LABEL_H
#define CSFGUI_LABEL_H

#include <CSFGUI/Export.h>
#include <CSFGUI/Types.h>

CSFGUI_API sfgLabel* sfgLabel_Create(const char* Text);
CSFGUI_API void sfgLabel_Destroy(sfgLabel* Label);
CSFGUI_API sfBool sfgLabel_GetLineWrap(sfgLabel* Label);
CSFGUI_API const char* sfgLabel_GetText(sfgLabel* Label);
CSFGUI_API const char* sfgLabel_GetWrappedText(sfgLabel* Label);
CSFGUI_API void sfgLabel_SetLineWrap(sfgLabel* Label, sfBool Wrap);
CSFGUI_API void sfgLabel_SetText(sfgLabel* Label, const char* Text);
CSFGUI_API sfgWidget* sfgLabel_GetBaseWidget(sfgLabel* Label);
CSFGUI_API sfBool sfgLabel_IsEqual(sfgLabel* A, sfgLabel* B);

#endif
