#ifndef CSFGUI_RADIOBUTTON_H
#define CSFGUI_RADIOBUTTON_H

#include <CSFGUI/Export.h>
#include <CSFGUI/Types.h>

CSFGUI_API sfgRadioButton* sfgRadioButton_Create(const char* Label, sfgRadioButtonGroup* RadioButtonGroup);
CSFGUI_API void sfgRadioButton_Destroy(sfgRadioButton* RadioButton);
CSFGUI_API sfgCheckButton* sfgRadioButton_GetBaseCheckButton(sfgRadioButton* RadioButton);
CSFGUI_API sfBool sfgRadioButton_IsEqual(sfgRadioButton* A, sfgRadioButton* B);
CSFGUI_API sfgRadioButtonGroup* sfgRadioButton_GetGroup(sfgRadioButton* RadioButton);
CSFGUI_API void sfgRadioButton_SetGroup(sfgRadioButton* RadioButton, sfgRadioButtonGroup* RadioButtonGroup);

#endif
