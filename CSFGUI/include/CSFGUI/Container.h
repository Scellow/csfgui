#ifndef CSFGUI_CONTAINER_H
#define CSFGUI_CONTAINER_H

#include <CSFGUI/Export.h>
#include <CSFGUI/Types.h>

CSFGUI_API void sfgContainer_Destroy(sfgContainer* Container);
CSFGUI_API void sfgContainer_Add(sfgContainer* Container, sfgWidget* Widget);
CSFGUI_API sfgWidget* sfgContainer_GetChildByIndex(sfgContainer* Container, sfInt32 Index);
CSFGUI_API sfInt32 sfgContainer_GetChildrenCount(sfgContainer* Container);
CSFGUI_API void sfgContainer_HandleChildInvalidate(sfgContainer* Container, sfgWidget* Widget);
CSFGUI_API sfBool sfgContainer_IsChild(sfgContainer* Container, sfgWidget* Widget);
CSFGUI_API void sfgContainer_Remove(sfgContainer* Container, sfgWidget* Widget);
CSFGUI_API void sfgContainer_RemoveAll(sfgContainer* Container);
CSFGUI_API sfgWidget* sfgContainer_GetBaseWidget(sfgContainer* Container);
CSFGUI_API sfBool sfgContainer_IsEqual(sfgContainer* A, sfgContainer* B);

#endif
