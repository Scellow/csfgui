#include <CSFGUI/Bin.h>
#include <CSFGUI/BinStruct.h>
#include <CSFGUI/WidgetStruct.h>
#include <CSFGUI/ContainerStruct.h>

void sfgBin_Destroy(sfgBin* Bin)
{
    delete Bin;
}

sfgWidget* sfgBin_GetChild(sfgBin* Bin)
{
    auto childptr = Bin->Handle->GetChild();
    if (childptr)
    {
        sfgWidget* widget = new sfgWidget;
        widget->Handle = childptr;
        return widget;
    }
    else return nullptr;
}

sfgContainer* sfgBin_GetBaseContainer(sfgBin* Bin)
{
    sfgContainer* container = new sfgContainer;
    container->Handle = Bin->Handle;
    return container;
}

sfBool sfgBin_IsEqual(sfgBin* A, sfgBin* B)
{
    return A->Handle == B->Handle;
}
