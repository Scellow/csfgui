#include <CSFGUI/Box.h>
#include <CSFGUI/BoxStruct.h>
#include <CSFGUI/WidgetStruct.h>
#include <CSFGUI/ContainerStruct.h>

sfgBox* sfgBox_Create(sfgBoxOrientation Orientation, float Spacing)
{
    sfgBox* box = new sfgBox;
    box->Handle = sfg::Box::Create(static_cast<sfg::Box::Orientation>(Orientation), Spacing);
    return box;
}

void sfgBox_Destroy(sfgBox* Box)
{
    delete Box;
}

float sfgBox_GetSpacing(sfgBox* Box)
{
    return Box->Handle->GetSpacing();
}

void sfgBox_Pack(sfgBox* Box, sfgWidget* Widget, sfBool Expand, sfBool Fill)
{
    Box->Handle->Pack(Widget->Handle, static_cast<bool>(Expand), static_cast<bool>(Fill));
}

void sfgBox_PackStart(sfgBox* Box, sfgWidget* Widget, sfBool Expand, sfBool Fill)
{
    Box->Handle->PackStart(Widget->Handle, static_cast<bool>(Expand), static_cast<bool>(Fill));
}

void sfgBox_PackEnd(sfgBox* Box, sfgWidget* Widget, sfBool Expand, sfBool Fill)
{
    Box->Handle->PackEnd(Widget->Handle, static_cast<bool>(Expand), static_cast<bool>(Fill));
}

void sfgBox_SetSpacing(sfgBox* Box, float Spacing)
{
    Box->Handle->SetSpacing(Spacing);
}

sfBool sfgBox_IsEqual(sfgBox* A, sfgBox* B)
{
    return A->Handle == B->Handle;
}

sfgContainer* sfgBox_GetBaseContainer(sfgBox* Box)
{
    sfgContainer* container = new sfgContainer;
    container->Handle = Box->Handle;
    return container;
}
