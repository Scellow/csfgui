#include <CSFGUI/Signal.h>
#include <CSFGUI/SignalStruct.h>

void sfgSignal_Destroy(sfgSignal* Signal)
{
    delete Signal;
}

sfUint32 sfgSignal_ConnectCallback(sfgSignal* Signal, sfgSignalCallback Callback)
{
    return Signal->Handle->Connect(Callback);
}

void sfgSignal_DisconnectCallback(sfgSignal* Signal, sfUint32 CallbackID)
{
    Signal->Handle->Disconnect(CallbackID);
}
