#include <CSFGUI/RadioButtonGroup.h>
#include <CSFGUI/RadioButtonGroupStruct.h>
#include <CSFGUI/RadioButtonStruct.h>

sfgRadioButtonGroup* sfgRadioButtonGroup_Create()
{
    sfgRadioButtonGroup* radiobuttongroup = new sfgRadioButtonGroup;
    radiobuttongroup->Handle = sfg::RadioButton::RadioButtonGroup::Create();
    return radiobuttongroup;
}

void sfgRadioButtonGroup_Destroy(sfgRadioButtonGroup* RadioButtonGroup)
{
    delete RadioButtonGroup;
}

sfBool sfgRadioButtonGroup_IsEqual(sfgRadioButtonGroup* A, sfgRadioButtonGroup* B)
{
    return A->Handle == B->Handle;
}

sfgRadioButton* sfgRadioButtonGroup_GetMemberByIndex(sfgRadioButtonGroup* RadioButtonGroup, sfInt32 Index)
{
    auto radiobuttonptr = RadioButtonGroup->Members[Index].lock();
    if (radiobuttonptr)
    {
        sfgRadioButton* radiobutton = new sfgRadioButton;
        radiobutton->Handle = radiobuttonptr;
        return radiobutton;
    }
    else return nullptr;
}

sfInt32 sfgRadioButtonGroup_GetMemberCount(sfgRadioButtonGroup* RadioButtonGroup)
{
    RadioButtonGroup->Members.clear();
    RadioButtonGroup->Members.reserve(RadioButtonGroup->Handle->GetMembers().size());
    for (auto radiobuttonptr : RadioButtonGroup->Handle->GetMembers())
    {
        RadioButtonGroup->Members.push_back(radiobuttonptr);
    }
    return RadioButtonGroup->Members.size();
}
