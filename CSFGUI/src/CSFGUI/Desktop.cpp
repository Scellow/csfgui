#include <CSFGUI/Desktop.h>
#include <CSFGUI/DesktopStruct.h>
#include <CSFGUI/WidgetStruct.h>
#include <CSFGUI/EngineStruct.h>
#include <CSFGUI/ConvertBackEvent.h>

sfgDesktop* sfgDesktop_Create()
{
    return new sfgDesktop;
}

void sfgDesktop_Destroy(sfgDesktop* Desktop)
{
    delete Desktop;
}

void sfgDesktop_Add(sfgDesktop* Desktop, sfgWidget* Widget)
{
    Desktop->Handle.Add(Widget->Handle);
}

void sfgDesktop_BringToFront(sfgDesktop* Desktop, sfgWidget* Widget)
{
    Desktop->Handle.BringToFront(Widget->Handle);
}

sfgEngine* sfgDesktop_GetEngine(sfgDesktop* Desktop)
{
    sfgEngine* engine = new sfgEngine;
    engine->Handle = &Desktop->Handle.GetEngine();
    return engine;
}

void sfgDesktop_HandleEvent(sfgDesktop* Desktop, sfEvent* Event)
{
    sf::Event evt;
    ConvertBackEvent(*Event, &evt);
    Desktop->Handle.HandleEvent(evt);
}

sfBool sfgDesktop_LoadThemeFromFile(sfgDesktop* Desktop, const char* Filename)
{
    return Desktop->Handle.LoadThemeFromFile(Filename);
}

void sfgDesktop_Refresh(sfgDesktop* Desktop)
{
    Desktop->Handle.Refresh();
}

void sfgDesktop_Remove(sfgDesktop* Desktop, sfgWidget* Widget)
{
    Desktop->Handle.Remove(Widget->Handle);
}

void sfgDesktop_RemoveAll(sfgDesktop* Desktop)
{
    Desktop->Handle.RemoveAll();
}

void sfgDesktop_Update(sfgDesktop* Desktop, float Seconds)
{
    Desktop->Handle.Update(Seconds);
}
