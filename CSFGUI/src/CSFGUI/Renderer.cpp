#include <CSFGUI/Renderer.h>
#include <CSFGUI/RendererStruct.h>

void sfgRenderer_Destroy(sfgRenderer* Renderer)
{
    delete Renderer;
}

sfBool sfgRenderer_IsEqual(sfgRenderer* A, sfgRenderer* B)
{
    return &A->Handle == &B->Handle;
}
