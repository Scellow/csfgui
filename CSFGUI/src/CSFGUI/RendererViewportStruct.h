#ifndef CSFGUI_RENDERERVIEWPORTSTRUCT_H
#define CSFGUI_RENDERERVIEWPORTSTRUCT_H

#include <SFGUI/RendererViewport.hpp>

struct sfgRendererViewport
{
    sfg::RendererViewport::Ptr Handle;
};

#endif
