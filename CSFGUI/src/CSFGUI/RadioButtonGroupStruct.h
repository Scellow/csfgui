#ifndef CSFGUI_RAIDOBUTTONGROUPSTRUCT_H
#define CSFGUI_RADIOBUTTONGROUPSTRUCT_H

#include <SFGUI/RadioButton.hpp>

struct sfgRadioButtonGroup
{
    sfg::RadioButton::RadioButtonGroup::Ptr Handle;
    std::vector<std::weak_ptr<sfg::RadioButton>> Members;
};

#endif
