#ifndef CSFGUI_SFGUISTRUCT_H
#define CSFGUI_SFGUISTRUCT_H

#include <SFGUI/SFGUI.hpp>

struct sfgSFGUI
{
    sfg::SFGUI Handle;
};

#endif
