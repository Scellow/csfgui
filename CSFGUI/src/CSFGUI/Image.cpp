#include <CSFGUI/Image.h>
#include <CSFGUI/ImageStruct.h>
#include <CSFGUI/WidgetStruct.h>
#include <SFML/Graphics/ImageStruct.h>

sfgImage* sfgImage_Create(sfImage* Image)
{
    sfgImage* image = new sfgImage;
    image->Handle = sfg::Image::Create(Image->This);
    return image;
}

void sfgImage_Destroy(sfgImage* Image)
{
    delete Image;
}

void sfgImage_GetImage(sfgImage* Image, sfImage* SFImage)
{
    SFImage->This = Image->Handle->GetImage();
}

void sfgImage_SetImage(sfgImage* SFGImage, sfImage* SFImage)
{
    SFGImage->Handle->SetImage(SFImage->This);
}

sfgWidget* sfgImage_GetBaseWidget(sfgImage* Image)
{
    sfgWidget* widget = new sfgWidget;
    widget->Handle = Image->Handle;
    return widget;
}

sfBool sfgImage_IsEqual(sfgImage* A, sfgImage* B)
{
    return A->Handle == B->Handle;
}
