#ifndef CSFGUI_BINSTRUCT_H
#define CSFGUI_BINSTRUCT_H

#include <SFGUI/Bin.hpp>

struct sfgBin
{
    sfg::Bin::Ptr Handle;
};

#endif
