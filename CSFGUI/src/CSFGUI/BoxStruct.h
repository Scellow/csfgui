#ifndef CSFGUI_BOXSTRUCT_H
#define CSFGUI_BOXSTRUCT_H

#include <SFGUI/Box.hpp>

struct sfgBox
{
    sfg::Box::Ptr Handle;
};

#endif
