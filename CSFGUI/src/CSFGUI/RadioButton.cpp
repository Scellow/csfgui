#include <CSFGUI/RadioButton.h>
#include <CSFGUI/RadioButtonStruct.h>
#include <CSFGUI/CheckButtonStruct.h>
#include <CSFGUI/RadioButtonGroupStruct.h>

sfgRadioButton* sfgRadioButton_Create(const char* Label, sfgRadioButtonGroup* RadioButtonGroup)
{
    sfgRadioButton* radiobutton = new sfgRadioButton;
    if (RadioButtonGroup) radiobutton->Handle = sfg::RadioButton::Create(Label, RadioButtonGroup->Handle);
    else radiobutton->Handle = sfg::RadioButton::Create(Label);
    return radiobutton;
}

void sfgRadioButton_Destroy(sfgRadioButton* RadioButton)
{
    delete RadioButton;
}

sfgCheckButton* sfgRadioButton_GetBaseCheckButton(sfgRadioButton* RadioButton)
{
    sfgCheckButton* checkbutton = new sfgCheckButton;
    checkbutton->Handle = RadioButton->Handle;
    return checkbutton;
}

sfBool sfgRadioButton_IsEqual(sfgRadioButton* A, sfgRadioButton* B)
{
    return A->Handle == B->Handle;
}

sfgRadioButtonGroup* sfgRadioButton_GetGroup(sfgRadioButton* RadioButton)
{
    sfgRadioButtonGroup* radiobuttongroup = new sfgRadioButtonGroup;
    radiobuttongroup->Handle = RadioButton->Handle->GetGroup();
    return radiobuttongroup;
}

void sfgRadioButton_SetGroup(sfgRadioButton* RadioButton, sfgRadioButtonGroup* RadioButtonGroup)
{
    RadioButton->Handle->SetGroup(RadioButtonGroup->Handle);
}
